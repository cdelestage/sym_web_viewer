# SYM Web Viewer

A simple tool to visualize and analyze Spot Your Mood data.

Created in html/css/javascript, for full compatibility to pretty much all OSes.

Libraries used:
- [Bootstrap](https://github.com/twbs/bootstrap)
- [JQuery](https://github.com/jquery/jquery)
- [Konva](https://github.com/konvajs/konva)

## Usage

### Data importation

You can import SYM data from a simple text file with the following format :
1 datum per line, fields separated by a single space, timestamp in unix time ms, in this order :

    activity username x_coordinate y_coordinate timestamp

### Data processing

You can display single activity data for a single user or activity data for all users, filtered or not by date or date span.
It is also possible to layer any data selection on the same diagram.

You can compute and display the deltas for the currently displayed data.
Be aware that Deltas imply a 1/2 scale on the diagram.

### Exports

You can save as an image any diagram you generate with your data with the side annotations.
It will be treated by your browser as any downloaded file regarding your preferences.
Generally, by default, it will be saved to your Downloads folder.
It is - not yet - possible to customize the file name, so be wary of file management from your side.

## Contact

Feel free to contact me for any query regarding this software or the Spot Your Mood project.
Get my email address through my [information page on my university lab website](https://mica.u-bordeaux-montaigne.fr/delestage-charles-alexandre/).

## Roadmap

- Add heatmaps generation
- Add csv files support (import/export)
- Make tutorials
- Installer for Windows/OSX

## Cite this work

ISO 670 format:

DELESTAGE, Charles-Alexandre, 2023. SYM Web Viewer [en ligne]. [Browser-based]. 2023. Bordeaux, France. Disponible à l’adresse : https://gitlab.huma-num.fr/cdelestage/sym_web_viewer

Bibtex :

    {
        delestage_sym_2023,
        address = {Bordeaux, France},
        title = {{SYM} {Web} {Viewer}},
        copyright = {BSD-3 Licence},
        url = {https://gitlab.huma-num.fr/cdelestage/sym_web_viewer},
        author = {Delestage, Charles-Alexandre},
        year = {2023},
    }

