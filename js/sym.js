/***
 *  Spot Your Mood Viewer
 *  Version 1.0
 *
 *  Spot Your Mood created by Willy Yvart & Charles-Alexandre Delestage
 *  Programming by Charles-Alexandre Delestage
 *
 *  sym.js
 *
 *  Handles the Valence-Arousal display and everything canvas-related
 */

let maxWidth = window.innerWidth;
let maxHeight = window.innerHeight;

let pool = [];
let rect_offset_count = 0;

let stage = new Konva.Stage({
    container: 'container',   // id of container <div>
    width: maxWidth,
    height: maxHeight,
    x: 0,
    y: 0
});

let layer = new Konva.Layer();

let ratio = maxWidth > maxHeight ? 1376 / maxHeight : 1200 / maxWidth;
let xOffset = maxWidth > maxHeight ? (maxWidth - 1200 / ratio) / 2 : 0;
let yOffset = maxHeight > maxWidth ? (maxHeight - 1376 / ratio) / 2 : 0;

// raw data of the sequences, for further data processing (deltas, heatmaps)
let sequences = [];

let valaro = new Image();
valaro.src = 'res/valaro.png';
valaro.onload = function () {
    let valaroWidth, valaroHeight;
    if (maxHeight > maxWidth) {
        valaroWidth = maxWidth;
        valaroHeight = 1376 / ratio;
    } else {
        valaroWidth = 1200 / ratio;
        valaroHeight = maxHeight;
    }

    let img = new Konva.Image({
        x: xOffset,
        y: yOffset,
        image: valaro,
        width: valaroWidth,
        height: valaroHeight
    });

    layer.add(img);
};

/**
 * Draws a sequence of SYMData with vectors
 * @param {SYMData[]} sequence
 * @param updateSequences if false, used for internal delta calculations
 */
function drawSequence(sequence, updateSequences) {
    if (updateSequences)    sequences.push(sequence);
    let color = get_random_hex_color();
    let prev = -1;
    let points = [];
    let vectors = [];
    for (let i = 0; i < sequence.length; i++) {
        if (prev !== -1) prev = Math.round(sequence[i].timestamp/1000 - prev);
        else prev = 0;
        // TODO do something useful of the extra text per point
        points = points.concat(drawPoint(sequence[i], color, prev.toString()));
        prev = sequence[i].timestamp/1000;
    }

    for (let j = 0; j < sequence.length; j++) {
        if (j !== 0) {
            if (Math.pow(sequence[j-1].x - sequence[j].x, 2) > 0.5 && Math.pow(sequence[j-1].y - sequence[j].y, 2) > 0.5) {
                vectors = vectors.concat(drawVector(
                    parseInt(sequence[j - 1].x),
                    parseInt(sequence[j].x),
                    parseInt(sequence[j - 1].y),
                    parseInt(sequence[j].y),
                    color));
            }
        }
    }

    let rect = new Konva.Rect({
        x: 20,
        y: 100 + 30 * rect_offset_count,
        width: 50,
        height: 20,
        fill: color,
        stroke: 'black',
        strokeWidth: 2,
    });

    let temp = sequence[0];
    let text_prefab = [];
    console.log(temp);
    if (params.img_export.name) text_prefab.push(temp.user);
    if (params.img_export.activity) text_prefab.push(temp.activity);
    if (params.img_export.date) text_prefab.push(temp.timestamp.toDateString());
    if (params.img_export.time) text_prefab.push(temp.timestamp.toLocaleTimeString());
    let user_text = new Konva.Text({
        x: 80,
        y: 100 + 30 * rect_offset_count++,
        fontFamily: 'Sans',
        fontSize: 20,
        text: text_prefab.join(' '),
        fill: 'black',
    });

    rect.on('touchend mouseup', function () {
        console.log(sequence[0].user, sequence[0].activity);
        if (user_text.attrs.fill === 'black') {
            user_text.fill('grey');
            rect.fill('grey');
            points.forEach(e => {
                e.opacity(0);
            });
            vectors.forEach(e => {
                e.opacity(0);
            });
        } else {
            user_text.fill('black');
            rect.fill(color);
            points.forEach(e => {
                e.opacity(1);
            });
            vectors.forEach(e => {
                e.opacity(1);
            });
        }
    });

    pool.push(rect);
    pool.push(user_text);

    layer.add(rect);
    layer.add(user_text);

    layer.draw();
}

/**
 * Draws a vector between two points
 * @param {int} x1
 * @param {int} x2
 * @param {int} y1
 * @param {int} y2
 * @param {string} color
 */
function drawVector(x1, x2, y1, y2, color) {
    let corrA = calculateScreenCoordinates(x1, y1);
    let corrB = calculateScreenCoordinates(x2, y2);

    let arrow = new Konva.Arrow({
        points: corrA.concat(corrB),
        pointerLength: 10,
        pointerWidth: 10,
        fill: color,
        stroke: 'black',
        strokeWidth: 2,
    });

    pool.push(arrow);

    layer.add(arrow);
    layer.draw();
    // console.log('connected');

    return arrow;
}

/**
 * Calculates on screen coordinates for a (x, y) couple
 *
 * It's a VIF*, modify only to fix the screen correlation issue
 * *Very Important Function
 * @param {int} x
 * @param {int} y
 * @returns {number[]} tuples are life, but it's javascript, i'm sad
 */
function calculateScreenCoordinates(x, y) {
    let corr = maxWidth > maxHeight ? 310 / stage.height() : 270 / stage.width();
    let xf = (x / corr) + (stage.width() / 2);
    let yf = (-y / corr) + (stage.height() / 2);
    return [xf, yf]
}

/**
 * Draws a single SYM pont on screen with extra text
 *
 * Another core function, do not modify until the end of the world, or may cause it
 * @param {SYMData} data
 * @param {string} color
 * @param {string} extra
 */
function drawPoint(data, color, extra) {
    let corr = calculateScreenCoordinates(data.x, data.y);
    let xf = corr[0];
    let yf = corr[1];

    let circle = new Konva.Circle({
        x: xf,
        y: yf,
        radius: 15, // TODO: scale to page size
        fill: color,
        stroke: 'black',
        strokeWidth: 2,
        shadowOpacity: 0
    });

    let extraText = new Konva.Text({
        x: xf + 10,
        y: yf + 10,
        fontFamily: 'Calibri',
        fontSize: 10,
        text: extra,
        fill: 'black',
    });

    layer.add(extraText);
    pool.push(extraText);

    pool.push(circle);

    circle.on('mouseover', function () {
        $('#span_x').text('x: ' + data.x.toString());
        $('#span_y').text('y: ' + data.y.toString());
        $('#span_activity').text('activity: ' + data.activity);
        $('#span_user').text('user: ' + data.user);
        $('#span_timestamp').text('timestamp: ' + data.timestamp.toDateString() + ' ' + data.timestamp.toLocaleTimeString());
    });

    circle.on('mouseout', function () {
        $('#span_x').text('');
        $('#span_y').text('');
        $('#span_activity').text('');
        $('#span_user').text('');
        $('#span_timestamp').text('');
    });

    layer.add(circle);
    layer.draw();
    return [circle, extraText];
}

function deltas() {
    if (params.delta_display !== 'full')  purge(true);
    let gottenDeltas = function(res) {
        res.forEach(r => {
            let seq = [r];
            drawSequence(seq, false);
        });
    }
    let seqcat = [];
    if (params.delta_calc === 'full') seqcat = data_pool;
    else sequences.forEach(e => { seqcat = seqcat.concat(e) });
    getDeltas(seqcat, gottenDeltas);
}

function heatmap() {

}

function purge(keepSequences) {
    pool.forEach(e => e.remove());
    pool = [];
    if (!keepSequences) sequences = [];

    rect_offset_count = 0;
    layer.draw();
}

// add the layer to the stage
stage.add(layer);

// draw the image
layer.draw();