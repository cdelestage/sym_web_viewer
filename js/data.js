/***
 * SYM data file format
 * Activity Name X Y Timestamp
 *
 * As a text file
 */

/**
 * SYM data object
 *
 * Core function, do not touch it
 * @param {string} x
 * @param {string} y
 * @param timestamp
 * @param {string} activity
 * @param {string} user
 */
function SYMData(x, y, timestamp, activity, user) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    // trickery thanks to javascript's lazy typing
    if (typeof timestamp === "object") { // data inserted manually
        this.timestamp = timestamp;
    } else { // data imported from a file
        this.timestamp = new Date(parseInt(timestamp));
    }
    this.activity = activity;
    this.user = user;
}

/**
 * Checks pairs of A/B data and excludes the singletons
 * (Not used for now)
 * @param {SYMData[]} dataset
 */
function checkPairs(dataset) {
    let delta = 86399;  // a day in utc nanoseconds

    let results = [];
    let basesets = [...dataset];

    while (basesets.length > 0) {
        if (basesets.length === 1) {
            basesets.pop();
            break;
        }

        for (let i = 1; i < basesets.length; i++) {
            if(basesets[0].user === basesets[i].user && basesets[0].activity === basesets[0].activity) {
                if (Math.pow(basesets[0].timestamp - basesets[i].timestamp, 2) < Math.pow(delta, 2)) {
                    // store couple in results
                    results.push([basesets[0], basesets[i]])
                    // pop them out
                    basesets.splice(basesets.indexOf(basesets[i]), 1);
                    // skip to next
                    break;
                }
            }
        }
        basesets.shift();
    }
    return results;
}

/**
 * Loads data from file and sends back an array of SYMData
 *
 * SYM data file format
 * Activity Name X Y Timestamp
 * As a txt file in UTF-8
 *
 * @param {Blob} file
 * @param {function} callback
 */
function loadData(file, callback) {
    let result = [];
    let reader = new FileReader();
    reader.onload = (e) => {
        const f = e.target.result;
        const lines = f.split(/\r\n|\n/);
        lines.forEach(l => {
            let fields = l.split(' ');
            if (fields.length === 5) {
                let temp = new SYMData(fields[2],fields[3], fields[4], fields[0], fields[1]);
                result.push(temp);
            }
        });
        callback(result);
    }
    reader.readAsText(file);
}

/**
 * Save the current collection
 * TODO : does not work :(
 * SYM data file format
 * Activity Name X Y Timestamp
 * As a txt file in UTF-8
 *
 * @param {[SYMData]} collection
 */
function saveData(collection) {
    console.log("saving data...");
    let content = "";
    let textFileDL = function (text) {
        let data = new Blob([text], {type: 'text/plain'});
        let fileReady = new File([data], 'sym_data.txt');
        let link = document.createElement('a');
        link.download = 'sym_data.txt';
        link.href = fileReady;
        link.click();
    };
    collection.forEach(c => {
        let exported = [c.activity, c.user, c.x, c.y, c.timestamp.valueOf().toString()].join(' ');
        console.log(exported);
        content = content.concat(exported);
    });
    textFileDL(content);
}

/**
 * Gets list of the users in the collection
 * (UI only)
 * @param {SYMData[]} dataset
 * @param {function} callback
 */
function getUsers(dataset, callback) {
    let results = [];
    dataset.forEach(e => {
        //e = e[0];
        if (!results.includes(e.user)) {
            //console.log(e.user);
            results.push(e.user);
        }
    });
    callback(results);
}

/**
 * Gets data relevant to the specified user
 * @param {SYMData[]} dataset
 * @param {string} user
 * @param {function} callback
 */
function getDataForUser(dataset, user, callback) {
    let results = [];
    dataset.forEach(e => {
        //let temp = e[0];
        if (e.user === user)    results.push(e);
    });
    callback(results);
}

/**
 * Gets activities available for the specified user
 * (UI only)
 * @param {SYMData[]} dataset
 * @param {string} user
 * @param {function} callback
 */
function getActivityForUser(dataset, user, callback) {
    let results = [];
    dataset.forEach(e => {
        //e = e[0];
        if(!results.includes(e.activity)) {
            console.log(e.activity);
            results.push(e.activity);
        }
    });
    callback(results);
}

/**
 * Gets available languages list
 * @param {JSON} raw_data
 * @param {function} callback
 */
function getLanguagesList(raw_data, callback) {
    let results = [];
    for (let name in raw_data) {
        results.push([name, raw_data[name].language]);
    }
    callback(results);
}

/**
 * Gets data relevant to the specified activity
 * @param {SYMData[]} dataset
 * @param {string} activity
 * @param {function} callback
 */
function getDataActivityForUser(dataset, activity, callback) {
    let results = [];
    dataset.forEach(e => {
        //let temp = e[0];
        if (e.activity === activity) results.push(e);
        //console.log(e);
    });
    callback(results);
}

/**
 * Gets days available in the dataset
 * (UI only)
 * @param {SYMData[]} dataset
 * @param {function} callback
 */
function getDays(dataset, callback) {
    let results = [];
    dataset.forEach(e => {
        //e = e[0];
        let date = e.timestamp;
        const day = [date.getDate(), date.getMonth(), date.getFullYear()].join('/');
        if (!results.includes(day)) {
            results.push(day);
        }
    });
    results.sort();
    callback(results);
}

/**
 * Gets data relevant to the specified day
 * @param {SYMData[]} dataset
 * @param {string} date
 * @param {function} callback
 */
function getDataDays(dataset, date, callback) {
    let results = [];
    dataset.forEach(e => {
        const day = [e.timestamp.getDate(), e.timestamp.getMonth(), e.timestamp.getFullYear()].join('/');
        if (day === date)   results.push(e);
    });
    callback(results);
}

/**
 * Gets deltas for each user
 * @param {SYMData[]} dataset
 * @param {function} callback
 */
function getDeltas(dataset, callback) {
    let results = [];
    let users = [];

    // list users in data
    let list_users_delta = function(cb) {
        dataset.forEach(e => {
            if (!users.includes(e.user)) {
                users.push(e.user);
                //console.log(e.user);
            }
        });
        cb(users);
    }
    // search data for each user
    let res_users_delta = function() {
        users.forEach(user => {
            let user_data = [];
            // pool all data for the user
            dataset.forEach(data => {
                // sort data to get the earliest and last point
                if (data.user === user) {
                    if (user_data.length > 0) {
                        if (user_data.length === 1) {
                            if (user_data[0].timestamp < data.timestamp) {
                                user_data.push(data);
                            } else {
                                user_data = [data, user_data[0]];
                            }
                        } else {
                            if (user_data[0].timestamp > data.timestamp) {
                                user_data[0] = data;
                            }
                            if (user_data[1].timestamp < data.timestamp) {
                                user_data[1] = data;
                            }
                        }
                    } else {
                        user_data.push(data);
                    }
                }
            });
            // check if data suitable for delta (min 2, global delta)
            if (user_data.length > 1) {
                let last_index = user_data.length - 1;
                let delta = new SYMData(((user_data[last_index].x - user_data[0].x)/2).toString(),
                    ((user_data[last_index].y - user_data[0].y)/2).toString(),
                    user_data[last_index].timestamp,
                    'DELTA',
                    user);
                results.push(delta);
            }
        });
        results.sort();
        callback(results);
        return 0;
    }

    list_users_delta(res_users_delta);
}

/**
 * Sorts collections for activity overview
 * @param {SYMData[]} collection
 */
function displayActivity(collection) {
    console.log(collection);
    let users_found = function (users_list) {
        console.log(collection);
        let user_data_add = function (user_data) {
            console.log(collection);
            let user_date_ready = function (dates) {
                console.log(collection);
                dates.forEach(d => {
                    let display = function (final_data) {
                        console.log(final_data);
                        drawSequence(final_data, true);
                    }
                    getDataDays(user_data, d, display);
                });
            }
            getDays(user_data, user_date_ready);
        }
        users_list.forEach(e => {
            getDataForUser(collection, e, user_data_add);
        });
    }
    sortData(collection, function () {
        getUsers(collection, users_found);
    })
}

function getDataFromRange(dataset, start, end, callback) {
    let results = [];
    let start_comp = start.split('/');
    let start_date = new Date(start_comp[2], start_comp[1], start_comp[0], 0, 0, 0);
    let end_comp = end.split('/');
    let end_date = new Date(end_comp[2], end_comp[1], end_comp[0], 23, 59, 59);
    dataset.forEach(e => {
        let isBefore = e.timestamp < start_date;
        let isAfter = e.timestamp > end_date;

        if (!isBefore && !isAfter) {
            results.push(e);
            // console.log(e.timestamp, isBefore, isAfter, e);
        }
    });
    callback(results);
}

/**
 * Sort function for SYMData collections
 *
 * Core function, do not modify
 * @param collection
 * @param callback
 */
function sortData(collection, callback) {
    collection.sort(function (a, b) {
        if (a.activity === b.activity) {
            if (a.user === b.user) {
                return a.timestamp - b.timestamp;
            }
            return a.user - b.user;
        }
        return a.activity - b.activity;
    });
    callback(collection);
}

function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}